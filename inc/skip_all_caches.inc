<?php

use Drupal\skip_all_caches\SettingsCacheChecker;
use Symfony\Component\HttpFoundation\Request;

/**
 * Remove all Caches from settings.
 *
 * @param \Symfony\Component\HttpFoundation\Request $request
 *   A request object.
 * @param array $settings
 *   The current settings array.
 *
 * @return array
 *   The updated settings array.
 */
function checkRemoveAllCaches(Request $request, array $settings) : array {
  // Manually include class files since autoloader isn't ready yet.
  require_once __DIR__ . '/../src/RemoveCacheFromSettings.php';
  require_once __DIR__ . '/../src/SettingsCacheChecker.php';
  require_once __DIR__ . '/../src/Checker/ShouldSkipCacheInterface.php';
  require_once __DIR__ . '/../src/Checker/SkipCacheForPaths.php';

  // Add CLI checker to the settings.
  $settings['skip_all_caches_cli_checker'] = function($request, $settings) {
    return PHP_SAPI !== 'cli';
  };

  $settings_cache_checker = SettingsCacheChecker::create($settings);
  if ($settings_cache_checker->shouldSkipAllCache($request)) {
    $settings = $settings_cache_checker->getUpdatedSettings();
  }

  return $settings;
}
