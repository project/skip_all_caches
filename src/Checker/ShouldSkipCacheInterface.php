<?php


namespace Drupal\skip_all_caches\Checker;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface to Should Cache checker.
 */
interface ShouldSkipCacheInterface {

  /**
   * Should the request be cached?
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @param array $settings
   *   The current settings array.
   * @return bool
   *   Should the request skip cache?
   */
  public function __invoke(Request $request, array $settings) : bool;

  /**
   * Static Create class to allow injection.
   *
   * @param array $settings
   *   The current settings array
   *
   * @return \Drupal\skip_all_caches\Checker\ShouldSkipCacheInterface
   */
  public static function create(array $settings) : ShouldSkipCacheInterface;
}
