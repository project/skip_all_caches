<?php


namespace Drupal\skip_all_caches\Checker;


use Symfony\Component\HttpFoundation\Request;

class SkipCacheForPaths implements ShouldSkipCacheInterface {

  /**
   * {@inheritDoc}
   */
  public function __invoke(Request $request, array $settings): bool {
    $path = $request->getPathInfo();
    $allowed_paths = $settings['skip_all_caches_for_paths'] ?? [];
    $trimmed_path = ltrim($path, '/');
    if (in_array($trimmed_path, $allowed_paths)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * @inheritDoc
   */
  public static function create(array $settings): ShouldSkipCacheInterface {
    return new static();
  }
}
