<?php


namespace Drupal\skip_all_caches;

use Drupal\Core\Site\Settings;
use Drupal\skip_all_caches\Checker\ShouldSkipCacheInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * A Kernel to use with site Alerts.
 */
class SettingsCacheChecker {

  /**
   * @var RemoveCacheFromSettings
   */
  protected $removeCacheFromSettings;

  /**
   * @var array
   */
  protected $settings;

  /**
   * @var \Drupal\skip_all_caches\Checker\ShouldSkipCacheInterface[]
   */
  protected $skipCacheCheckers = [];

  /**
   * SiteAlertKernel constructor.
   *
   * @param \Drupal\skip_all_caches\RemoveCacheFromSettings $removeCacheFromSettings
   * @param array $settings
   */
  public function __construct(RemoveCacheFromSettings $removeCacheFromSettings, array $settings) {
    $this->removeCacheFromSettings = $removeCacheFromSettings;
    $this->settings = $settings;
  }

  public static function create(array $settings): SettingsCacheChecker {
    return new static(
      new RemoveCacheFromSettings(),
      $settings
    );
  }

  /**
   * Check if we are in Maintenance Mode.
   * This is set by manually including a maintenance mode settings file during deploy.
   *
   * Maintenance mode is normally set in the database which we don't have setup at this point.
   *
   * @return bool
   */
  public function skipCacheEnabled() : bool {
    return $this->settings['skip_all_caches_enabled'] ?? FALSE;
  }

  /**
   * Update the Settings to remove Cache.
   *
   * @return array
   *   The new settings array.
   */
  public function getUpdatedSettings() : array {
    return $this->removeCacheFromSettings->updateSettingsArray($this->settings);
  }

  /**
   * Should the current path check to skip all caches.
   *
   * @return bool
   *   Should we remove cache for a path.
   */
  public function shouldSkipAllCache(Request $request) : bool {
    if (!$this->skipCacheEnabled()) {
      return FALSE;
    }

    foreach ($this->getSkipCacheCheckers() as $checker_class) {
      $checker = $this->getCallable($checker_class);

      if (is_null($checker)) {
        continue;
      }

      if ($checker($request, $this->settings)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Get a callable class.
   *
   * @param $checker
   *   This should either be a callable function or ShouldSkipCacheInterface class.
   *
   * @return Callable|null
   *   Either the instance of the class or a callable function or null.
   */
  protected function getCallable($checker) : ?Callable {
    if (is_callable($checker)) {
      return $checker;
    }

    // Allow the reference of the settings which holds the function.
    if (!empty($this->settings[$checker]) && is_callable($this->settings[$checker])) {
      return $this->settings[$checker];
    }

    if ($checker instanceof ShouldSkipCacheInterface) {
      return $checker::create($this->settings);
    }

    return new $checker();
  }

  /**
   * Get the skip cache checkers.
   *
   * @return array
   *   Array of skip cache checkers
   */
  protected function getSkipCacheCheckers() : array {
    return $this->settings['skip_all_caches_checkers'] ?? [];
  }
}
