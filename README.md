At times we do not want to use the cache at all.  This module allows for totally remove the cache based upon certain circumstances.

## How it works

During the settings initialization, we have the opportunity to change the setting to all use a NULL backend service.

The following keys are used in the settings:

- **skip_all_caches_enabled**: Enable the skip cache check
- **skip_all_caches_checkers**: An array of checkers. The Checks are in order of the array. It could be one of the following:
  - Reference another setting which is a Callable a function.
  - Class Name
  - Instance of a class which has either `__invoke()` or implements `Drupal\skip_all_caches\Checker\ShouldSkipCacheInterface`

The following Checker Plugins are built in:

- **Drupal\skip_all_caches\Checker\SkipCacheForPaths**: Checks the settings `skip_all_caches_for_paths` for paths to skip cache on.
- **skip_all_caches_cli_checker**: A function which does not check skips if running via CLI

## Settings.php example

The settings.php file will need to be updated to call the Skip Cache checker.

Example update to settings.php

```php
<?php

use Symfony\Component\HttpFoundation\Request;

$settings['skip_all_caches_enabled'] = TRUE;
$settings['skip_all_caches_for_paths'] = [
  'ajax/site_alert',
];
$settings['skip_all_caches_checkers'] = [
  'skip_all_caches_cli_checker',
  'Drupal\skip_all_caches\Checker\SkipCacheForPaths',
];

// These need to be updated manually using the generate_cache_bins.sh script.
$settings['skip_all_cache_bins'] = [
  'bootstrap',
  'cache_rebuild_command',
  'cache_tags.invalidator',
  'config',
  'data',
  'default',
  'discovery',
  'discovery_migration',
  'dynamic_page_cache',
  'entity',
  'graphql.definitions',
  'graphql.results',
  'jsonapi_memory',
  'jsonapi_normalizations',
  'jsonapi_resource_types',
  'libraries',
  'menu',
  'migrate',
  'page',
  'render',
  'rest',
  'static',
  'toolbar',
];

include_once $app_root . '/modules/contrib/skip_all_caches/inc/skip_all_caches.inc';

$request = Request::createFromGlobals();
$settings = checkRemoveAllCaches($request, $settings);
```


