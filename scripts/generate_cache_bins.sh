#!/bin/bash

# See "Find cache bins" of https://www.drupal.org/node/2598914
# brew install gawk for osx
# Run this from Drupal Root

find . -type f -name *.services.yml | \
xargs sed -E -n  's/.*cache\.(.*):.*/\1/p' | \
grep -v "backend\|html.twig" | \
sort -u | \
gawk -vORS=\',\' '{ print $1 }' | \
sed "s/,'$//" | sed "s/^/'/"
